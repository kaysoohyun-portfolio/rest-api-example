import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {

  constructor(private http: HttpClient) { }

  getCatImage() {
    let headers = new HttpHeaders().set('api-key', environment.apiCat);
    return this.http.get('https://api.thecatapi.com/v1/images/search', { headers: headers });
  }
}
