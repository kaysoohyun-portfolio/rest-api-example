import { Component, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { EMPTY, Subject } from 'rxjs';
import { catchError, finalize, takeUntil } from 'rxjs/operators';
import { ApiServiceService } from '../service/api-service.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit, OnDestroy {
  private ngUnsubscribe = new Subject<void>();
  catImageSrc: string = '';
  showError: boolean = false;
  showSpinner: boolean = false;
  constructor(
    private service: ApiServiceService,
    public cdr: ChangeDetectorRef,
  ) { }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  onGetCat() {
    this.showSpinner = true;
    this.showError = false;
    this.service.getCatImage()
    .pipe(
      catchError((error) => {
        this.showError = true;
        return EMPTY;
      }),
      finalize(() => {
        this.showSpinner = false;
        this.cdr.detectChanges();
      }),
      takeUntil(this.ngUnsubscribe)
    )
    .subscribe((result: any) => {
      this.showError = false;
      this.catImageSrc = result[0].url;
    });
  }
}